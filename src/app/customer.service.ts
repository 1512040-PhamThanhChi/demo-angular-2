import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Customer } from './customer';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Accept: 'application/json'
  })
};

@Injectable({ providedIn: 'root' })
export class CustomerService {
  private customerUrl = 'http://localhost:26466/api/Customers'; // URL to web api

  constructor(private http: HttpClient) {}

  getCustomers(): Observable<Customer[]> {
    const url = `${this.customerUrl}/GetAllCustomers/`;
    return this.http.get<Customer[]>(url, httpOptions);
  }

  /** GET Customer by id */
  getCustomer(id: number): Observable<Customer> {
    const url = `${this.customerUrl}/GetCustomerByID/id=${id}`;
    return this.http.get<Customer>(url, httpOptions);
  }

  /* GET Customers whose NRIC contains search term */
  searchCustomersByNric(term: string): Observable<Customer[]> {
    if (!term.trim()) {
      // if not search term, return all Customers.
      return this.getCustomers();
    }

    const url = `${this.customerUrl}/nric=${term}`;
    return this.http.get<Customer[]>(url, httpOptions);
  }

  /* GET Customers whose name contains search term */
  searchCustomersByName(term: string): Observable<Customer[]> {
    if (!term.trim()) {
      // if not search term, return all Customers.
      return this.getCustomers();
    }

    const url = `${this.customerUrl}/GetCustomerByName/name=${term}`;
    return this.http.get<Customer[]>(url, httpOptions);
  }

  /* GET Customers whose phone contains search term */
  searchCustomersByPhone(term: string): Observable<Customer[]> {
    if (!term.trim()) {
      // if not search term, return all Customers.
      return this.getCustomers();
    }

    const url = `${this.customerUrl}/phone=${term}`;
    return this.http.get<Customer[]>(url, httpOptions);
  }

  ////////// Save methods //////////

  /** POST: add a new user to the server */
  addCustomer(customer: Customer): Observable<Customer> {
    const url = `${this.customerUrl}/AddCustomer`;
    return this.http.post<Customer>(url, customer, httpOptions);
  }

  /** POST: delete the user from the server */
  deleteCustomer(customer: any | number): Observable<Customer> {
    const id = typeof customer === 'number' ? customer : customer.Id;
    const url = `${this.customerUrl}/RemoveCustomer/?id=${id}&isdelete=true`;
    return this.http.post<Customer>(url, httpOptions);
  }

  /** PUT: update the user on the server */
  updateCustomer(customer: Customer): Observable<any> {
    const url = `${this.customerUrl}/UpdateCustomer`;
    return this.http.put(url, customer, httpOptions);
  }
}
